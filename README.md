#README / LEEME

*Práctica:* 08

*Unidad:* UD02. Hojas de Estilo en Cascada

*Módulo:* Diseño de Interfaces WEB

*Ciclo Formativo:* Desarrollo de Aplicaciones WEB

*Objetivo:* Animaciones

##Author / Autor

Alfredo Oltra (mail: [alfredo.ptcf@gmail.com](alfredo.ptcf@gmail.com) / twitter: [@aoltra](https://twitter.com/aoltra))

##License / Licencia

Creative Commons: [by-no-sa](http://es.creativecommons.org/blog/licencias/)
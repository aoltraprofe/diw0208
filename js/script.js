window.addEventListener("load", init, false);

/********************
 Inicializa valores 
 ********************/
function init() {
    var boton = document.getElementById("ejecutar");
    boton.addEventListener("click", runAnimation, false);
    
    // creo estrellas aleatoriamomente
    var estrellaG = document.getElementsByClassName("estrella__grande")[0];
    var estrellaP = document.getElementsByClassName("estrella__pequena")[0];
    
    var cielo = document.getElementsByClassName("cielo")[0];
    
    // estrellas grandes, entre 4 y 8
    for (cont = 0, nEG = Math.floor((Math.random() * 8) + 4); cont < nEG; cont++) {
        estrellaT = estrellaG.cloneNode(true);
        estrellaT.className += "estrella estrella__grande";
        estrellaT.style.top =  Math.floor((Math.random() * 190) + 1) + "px";
        estrellaT.style.left =  Math.floor((Math.random() * cielo.scrollWidth) + 1) + "px";
        
        estrellaT.style.animationDelay = Math.floor((Math.random() * 10) + 1) + "s";
        
        estrellaG.parentNode.appendChild(estrellaT);
    }
    
    // estrellas pequeñas, entre 4 y 15
    for (cont = 0, nEP = Math.floor((Math.random() * 15) + 4); cont < nEP; cont++) {
        estrellaT = estrellaP.cloneNode(true);
        estrellaT.className += "estrella estrella__pequena";
        estrellaT.style.top =  Math.floor((Math.random() * 190) + 1) + "px";
        estrellaT.style.left =  Math.floor((Math.random() * cielo.scrollWidth) + 1) + "px";
        
        estrellaT.style.animationDelay = Math.floor((Math.random() * 10) + 1) + "s";
        
        estrellaP.parentNode.appendChild(estrellaT);
    }
}

/************************
 Ejecuta la animación
 ************************/
function runAnimation() {
 
    var amanecible = document.getElementsByClassName("amanecible");
    
    for (cont = 0, len = amanecible.length; cont < len; cont++) {
        amanecible[cont].className+=" amanecer-atardecer";
    }
    
    var sol = document.getElementsByClassName("sol");    
    sol[0].className += " ciclo-sol";

    var estrellasG = document.getElementsByClassName("estrella__grande");
    for (cont = 0, len = estrellasG.length; cont < len; cont++) {
        estrellasG[cont].className += " ciclo-estrellas";
    }
    
    var estrellasP = document.getElementsByClassName("estrella__pequena");
    for (cont = 0, len = estrellasP.length; cont < len; cont++) {
        estrellasP[cont].className += " ciclo-estrellas";
    }
}